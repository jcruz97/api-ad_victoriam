include .env

rebuild:
	docker-compose up -d --build

cc:
	$(call exec, "php bin/console cache:clear")

install:
	$(call exec, "composer install")

composer:
	$(call exec, "composer require ${require}")

symcli:
	$(call exec, "php bin/console ${COMMAND}")

ssh:
	docker exec -it ${APP_NAME}_php bash


define exec
	docker exec -it ${APP_NAME}_php bash -c ${1}
endef