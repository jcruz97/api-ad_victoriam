<?php

namespace App\Repository;

use App\Entity\MainCharacter;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MainCharacter|null find($id, $lockMode = null, $lockVersion = null)
 * @method MainCharacter|null findOneBy(array $criteria, array $orderBy = null)
 * @method MainCharacter[]    findAll()
 * @method MainCharacter[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MainCharacterRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MainCharacter::class);
    }

    // /**
    //  * @return MainCharacter[] Returns an array of MainCharacter objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MainCharacter
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
