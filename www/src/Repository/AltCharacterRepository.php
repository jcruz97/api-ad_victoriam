<?php

namespace App\Repository;

use App\Entity\AltCharacter;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AltCharacter|null find($id, $lockMode = null, $lockVersion = null)
 * @method AltCharacter|null findOneBy(array $criteria, array $orderBy = null)
 * @method AltCharacter[]    findAll()
 * @method AltCharacter[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AltCharacterRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AltCharacter::class);
    }

    // /**
    //  * @return AltCharacter[] Returns an array of AltCharacter objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AltCharacter
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
