<?php

namespace App\Repository;

use App\Entity\GoldTracking;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method GoldTracking|null find($id, $lockMode = null, $lockVersion = null)
 * @method GoldTracking|null findOneBy(array $criteria, array $orderBy = null)
 * @method GoldTracking[]    findAll()
 * @method GoldTracking[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GoldTrackingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, GoldTracking::class);
    }

    // /**
    //  * @return GoldTracking[] Returns an array of GoldTracking objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?GoldTracking
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
